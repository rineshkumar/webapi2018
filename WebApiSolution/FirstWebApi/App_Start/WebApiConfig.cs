﻿using FirstWebApi.Exceptions;
using FirstWebApi.Security;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace FirstWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Formatters.Add(new ProductCsvFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //   config.Filters.Add(new ValidateModelAttribute());
            config.Filters.Add(new NotImplExceptionFilterAttribute());
            var traceWriter = config.EnableSystemDiagnosticsTracing();
            traceWriter.IsVerbose = true;
            traceWriter.MinimumLevel = TraceLevel.Debug;
            //config.Filters.Add(new AuthorizeAttribute());
            //config.Filters.Add(new CustomAuthorizationAttribute());

        }
    }
}
