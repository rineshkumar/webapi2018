﻿using FirstWebApi.Exceptions;
using FirstWebApi.Models;
using FirstWebApi.ValueProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ValueProviders;

namespace FirstWebApi.Controllers
{
    public class GeoPointController : ApiController
    {
        // GET: api/GeoPoint
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/GeoPoint/5
        public string Get([ValueProvider(typeof(GeoPointValueProviderFactory))]string point,int id)
        {
            return "value";
        }

        // POST: api/GeoPoint
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GeoPoint/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GeoPoint/5
        
        public HttpResponseMessage Delete(int id)
        {
            //throw new NotImplementedException();
            return Request.CreateErrorResponse(
                HttpStatusCode.NotImplemented, "Not implemented"); 
        }
    }
}
