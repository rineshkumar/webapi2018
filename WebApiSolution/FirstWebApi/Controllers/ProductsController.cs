﻿using FirstWebApi.Models;
using FirstWebApi.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace FirstWebApi.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[]
       {
            new Product { Id = 1, Name = "Tomato Soup", Category = "Groceries", Price = 1 },
            new Product { Id = 2, Name = "Yo-yo", Category = "Toys", Price = 3.75M },
            new Product { Id = 3, Name = "Hammer", Category = "Hardware", Price = 16.99M }
       };
        public IEnumerable<Product> GetAllProducts()
        {
            Configuration.Services.GetTraceWriter().Info(
           Request, "ProductsController", "Get the list of products.");
            return products;
        }
        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
        //public HttpResponseMessage GetProduct(int id)
        //{
        //    var product = new Product()
        //    { Id = id, Name = "Gizmo", Category = "Widgets", Price = 1.99M };

        //    var negotiator = this.Configuration.Services.GetContentNegotiator();
        //    var result = negotiator.Negotiate(
        //        typeof(Product),
        //        this.Request,
        //        this.Configuration.Formatters);
        //    if (result == null)
        //    {
        //        throw new HttpResponseException(new HttpResponseMessage { StatusCode = HttpStatusCode.NotAcceptable });
        //    }
        //    else
        //    {
        //        return new HttpResponseMessage
        //        {
        //            Content = new ObjectContent<Product>(product, result.Formatter, result.MediaType.MediaType)

        //        };
        //    }
        //}
        //[Route("~/api/products/anonymous")]
        //public object Get()
        //{
        //    return new
        //    {
        //        key = "key",
        //        values = new List<string> { "a", "b", "c" }
        //    };
        //}
        
        [CustomAuthorization]
        [Route("~/api/products/postAnyJson")]
        public IHttpActionResult PostLooseJson(JObject inputData)
        {
            string key = inputData["key"].ToString();
            if (User.IsInRole("Administrators"))
            {
                // ...
            }
            //for (int i = 0; i < inputData["values"].Count; i++)
            //{

            //}
            return Ok(key);
        }
        [ValidateModel]
        public IHttpActionResult Post(Product product)
        {
            //here we have moved the validation to the validate model filter
            return Ok();
        }
    }
}
