﻿using FirstWebApi.Models;
using System.Globalization;
using System.Web.Http.Controllers;
using System.Web.Http.ValueProviders;

namespace FirstWebApi.ValueProviders
{
    internal class GeoPointValueProvider : IValueProvider
    {
        private HttpActionContext actionContext;

        public GeoPointValueProvider(HttpActionContext actionContext)
        {
            this.actionContext = actionContext;
        }

        public bool ContainsPrefix(string prefix)
        {
            //Checks if information is available with us. 
            return "point".Equals(prefix);
        
        }

        public ValueProviderResult GetValue(string key)
        {

            return new ValueProviderResult("Hello World",key, CultureInfo.InvariantCulture);
        }
    }
}