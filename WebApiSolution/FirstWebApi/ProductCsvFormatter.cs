﻿using FirstWebApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace FirstWebApi
{
    public class ProductCsvFormatter : BufferedMediaTypeFormatter
    {
        public ProductCsvFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/csv"));

            SupportedEncodings.Add(new UTF8Encoding(encoderShouldEmitUTF8Identifier: false));
            SupportedEncodings.Add(Encoding.GetEncoding("iso-8859-1"));
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            //Supporting product and list of product 
            if (type == typeof(Product))
            {
                return true;
            }
            else
            {
                var productListType = typeof(IEnumerable<Product>);
                return productListType.IsAssignableFrom(type);
            }

        }

        public override void WriteToStream(Type type, object value, Stream writeStream, HttpContent content)
        {
            //1.Get a writer for the stream and start writing to the stream 
            var preferredEncoding = SelectCharacterEncoding(content.Headers);

            using (var writer = new StreamWriter(writeStream,preferredEncoding))
            {
                //2.Try casting to the supported type . 
                var productList = value as IEnumerable<Product>;
                if (productList != null)
                {
                    foreach (var item in productList)
                    {
                        writeItem(item, writer);
                    }
                }
                else
                {
                    var product = value as Product;
                    writeItem(product, writer);
                }
            }
        }

        private void writeItem(Product item, StreamWriter writer)
        {
            writer.WriteLine("{0}-{1}-{2}-{3}", item.Id, item.Name, item.Price, item.Category);
        }
    }
}