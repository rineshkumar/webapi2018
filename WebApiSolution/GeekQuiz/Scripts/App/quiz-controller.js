﻿angular.module('QuizApp', [])
    .controller('QuizCtrl', function ($scope, $http) {
        $scope.answered = false;
        $scope.title = "loading question...";
        $scope.options = [];
        $scope.correctAnswer = false;
        $scope.working = false;

        $scope.answer = function () {
            return $scope.correctAnswer ? 'correct' : 'incorrect';
        };

        $scope.nextQuestion = function () {
            $scope.working = true;
            $scope.answered = false;
            $scope.title = "loading question...";
            $scope.options = [];
            $http({
                method: 'GET',
                url: '/api/trivia'
            }).then(function (response) {
                var data = response.data;
                $scope.options = data.options;
                $scope.title = data.title;
                $scope.answered = false;
                $scope.working = false;
            }, function (error) {
                $scope.title = "Oops... something went wrong";
                $scope.working = false;
            });
            //$http.get("/api/trivia").success(function (data, status, headers, config) {
            //    $scope.options = data.options;
            //    $scope.title = data.title;
            //    $scope.answered = false;
            //    $scope.working = false;
            //}).error(function (data, status, headers, config) {
            //    $scope.title = "Oops... something went wrong";
            //    $scope.working = false;
            //});
        };

        $scope.sendAnswer = function (option) {
            $scope.working = true;
            $scope.answered = true;

            //$http.post('/api/trivia', { 'questionId': option.questionId, 'optionId': option.id }).success(function (data, status, headers, config) {
            //    $scope.correctAnswer = (data === true);
            //    $scope.working = false;
            //}).error(function (data, status, headers, config) {
            //    $scope.title = "Oops... something went wrong";
            //    $scope.working = false;
            //    });

            // $http({
            //    url: '/api/trivia',
            //    method: "POST",
            //    data: { 'questionId': option.questionId, 'optionId': option.id },
            //    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            //}).success(function (data, status, headers, config) {
            //    $scope.data = data; // how do pass this to $scope.persons?
            //}).error(function (data, status, headers, config) {
            //    $scope.status = status;
            //    });

             $http({
                 method: 'POST',
                 url: '/api/trivia',
                 data: { 'questionId': option.questionId, 'optionId': option.id }
             }).then(function (response) {
                 $scope.correctAnswer = (response.data === true);
                $scope.working = false;
             }, function (error) {
                 $scope.title = "Oops... something went wrong";
                 });


        };

    });