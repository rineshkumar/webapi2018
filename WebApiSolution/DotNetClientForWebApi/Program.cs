﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using WebApiWithData.Models;

namespace DotNetClientForWebApi
{

    class Program
    {
        public HttpClient client { get; set; }
        public Program()
        {
            client = new HttpClient(new SampleClientHandler(new HttpClientHandler()));
            client.BaseAddress = new Uri("http://localhost:49548/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        static void Main(string[] args)
        {

            Program p = new Program();
            var list = p.GetBookListAsync().Result;
            foreach (var item in list)
            {
                Console.WriteLine(item.Title);
            }
            //Create a book 
            //string url = p.AddNewBookAsync().Result;

            //Updating a book 
            //var book = p.GetBookAsync().Result;
            //book.Title = "test book udpated ";
            //p.updateBookAsync(book).Wait(); ;
            Console.ReadLine();

        }

        private async Task updateBookAsync(Book book)
        {
            var resonse = await client.PutAsJsonAsync<Book>("/api/books/5", book);

        }

        private async Task<string> AddNewBookAsync()
        {
            Book b = new Book();
            b.AuthorId = 1;
            b.Genre = "Comedy";
            b.Price = 10;
            b.Title = "test book ";
            b.Year = 2018;
            var response = await client.PostAsJsonAsync<Book>("api/books", b);
            return response.Headers.Location.ToString();
        }

        //Reading a book 
        private async Task<List<Book>> GetBookListAsync()
        {
            var path = "api/Books";
            var response = await client.GetAsync(path);
            List<Book> bookList = null;
            if (response.IsSuccessStatusCode)
            {
                bookList = await response.Content.ReadAsAsync<List<Book>>();
            }
            return bookList;
        }
        private async Task<Book> GetBookAsync()
        {
            var path = "api/Books/5";
            var response = await client.GetAsync(path);
            Book book = null;
            if (response.IsSuccessStatusCode)
            {
                book = await response.Content.ReadAsAsync<Book>();
            }
            return book;
        }
    }
}
