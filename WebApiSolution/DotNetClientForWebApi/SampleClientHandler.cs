﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DotNetClientForWebApi
{
    class SampleClientHandler : DelegatingHandler
    {
        private HttpClientHandler httpClientHandler;

        public SampleClientHandler(HttpClientHandler httpClientHandler) : base(httpClientHandler)
        {
            this.httpClientHandler = httpClientHandler;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            Console.WriteLine("=========Handler Active ===============");
            return base.SendAsync(request, cancellationToken);
        }
    }
}
